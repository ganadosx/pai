<?php

require 'Routing.php';

$path = $_SERVER['REQUEST_URI'];
$path = trim($path, '/');
$path = parse_url($path, PHP_URL_PATH);

Router::get('', 'DefaultController');
Router::get('map', 'DefaultController');
Router::get('settings', 'DefaultController');
Router::get('routes', 'RoutesController');
Router::get('friends', 'FriendsController');
Router::get('news', 'NewsController');

Router::post('login', 'SecurityController');
Router::post('logout', 'SecurityController');
Router::post('uploadImage', 'UploadPersonImageController');
Router::post('register', 'SecurityController');

Router::post('search', 'FriendsController');

Router::run($path);