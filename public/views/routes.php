<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/routes.css">
    <script src="https://kit.fontawesome.com/ac5ad3ed5c.js" crossorigin="anonymous"></script>
    <title>PROJECTS</title>
</head>
<body>
<div class="base-container">
    <nav>
        <img src="public/img/logo.svg">
        <ul>
            <hr id="hr-under-logo">
            <li>
                <i class="fas fa-route"></i>
                <a href="/routes" class="button">Choose a route</a>
            </li>
            <li>
                <i class="fas fa-map"></i>
                <a href="/map" class="button">Map</a>
            </li>
            <li>
                <i class="fas fa-user-friends"></i>
                <a href="/friends" class="button">Friends</a>
            </li>
            <li>
                <i class="fas fa-bell"></i>
                <a href="/news" class="button">News</a>
            </li>
            <hr id="hr-under-menu">
            <li>
                <i class="fas fa-cog"></i>
                <a href="/settings" class="button">Settings</a>
            </li>
        </ul>
    </nav>
    <main>
        <header>
            <div class="search-bar">
                <form>
                    <input placeholder="find route">
                </form>
            </div>
            <?php if (strcmp($_COOKIE['role'], "ADMIN") == 0)
                echo "<div class='add-route'>   
                    <button id='add-route-button'>
                    <a href='/settings' id ='add-route-link'</a>
                     <i class='fas fa-plus'></i>add route
                    </button> 
                </div>"
            ?>
        </header>
        <section class="routes">
            <?php foreach ($allRoutes->getRoutes() as $route): ?>
                <div id="route-<?= $route->getId() ?>">
                    <div>
                        <h2><?= $route->getTitle() ?></h2>
                        <p><?= $route->getDescription() ?></p>
                    </div>
                    <img src="public/uploads/routes/<?= $route->getImageurl() ?>">
                </div>
            <?php endforeach; ?>
        </section>
    </main>
</div>

</body>