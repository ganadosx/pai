<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/friends.css">

    <script src="https://kit.fontawesome.com/ac5ad3ed5c.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="public/js/friendsSearch.js" defer></script>

    <title>FRIENDS</title>
</head>
<body>
<div class="base-container">
    <nav>
        <img src="public/img/logo.svg">
        <ul>
            <hr id="hr-under-logo">
            <li>
                <i class="fas fa-route"></i>
                <a href="/routes" class="button">Choose a route</a>
            </li>
            <li>
                <i class="fas fa-map"></i>
                <a href="/map" class="button">Map</a>
            </li>
            <li>
                <i class="fas fa-user-friends"></i>
                <a href="/friends" class="button">Friends</a>
            </li>
            <li>
                <i class="fas fa-bell"></i>
                <a href="/news" class="button">News</a>
            </li>
            <hr id="hr-under-menu">
            <li>
                <i class="fas fa-cog"></i>
                <a href="/settings" class="button">Settings</a>
            </li>
        </ul>
    </nav>
    <main>
        <header>
            <div class="search-bar">
                <input placeholder="find friend">
            </div>
            <div class="add-friend">
                <button id="add-friend-button" disabled><i class="fas fa-plus"></i>add friend</button>
            </div>
        </header>
        <section class="friends">
            <?php foreach ($allFriends->getFriends() as $friend): ?>
                <div id="friend-1">
                    <div>
                        <h2><?= $friend->getFirstName() . " " . $friend->getLastName() ?></h2>
                        <h3><?= $friend->getEmail() ?></h3>
                    </div>
                    <img src="<?= $friend->getImageUrl() ?>" alt="person.img">
                </div>
            <?php endforeach; ?>
        </section>
    </main>
</div>

</body>

<template id="friends-template">
    <div id="">
        <div>
            <h2></h2>
            <h3></h3>
        </div>
        <img src="" alt="person.img">
    </div>
</template>