<?php

require_once 'src/repository/Repository.php';
require_once 'src/models/user/authentication/User.php';
require_once 'src/models/user/friends/Friends.php';

class FriendsRepository extends Repository
{
    public function getFriends(string $email): Friends
    {
        $stmt = $this->database->connect()->prepare(
            '
                    select email, firstname, lastname, imageurl from users
                    join users_friendship uf on users.id = uf.id_friend
                    where uf.id_friend in
                      (select id_friend from users_friendship
                      where id_user = (select id from users where email = :email))
            '
        );
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();
        $friends = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $allFriends = new Friends();

        if ($friends == false) {
            return $allFriends;
        }

        foreach ($friends as $row) {
            $allFriends->addFriend(new User(
                    $row['email'],
                    '',
                    $row['firstname'],
                    $row['lastname'],
                    $row['imageurl'])
            );
        }

        return $allFriends;
    }

    public function getFriendsByTitle(string $email, string $searchString)
    {
        $searchString = '%' . strtolower($searchString) . '%';

        $stmt = $this->database->connect()->prepare('
            select email, firstname, lastname, imageurl
            from users
                     join users_friendship uf on users.id = uf.id_friend
            where uf.id_friend in
                  (select id_friend
                   from users_friendship
                   where id_user = (select id from users where email = :email))
            and LOWER(concat(firstname, lastname)) like :searchString;
        ');

        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':searchString', $searchString, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}