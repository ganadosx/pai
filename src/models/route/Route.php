<?php

require_once 'src/models/route/coordinate/Coordinates.php';

class Route
{
    private string $id;
    private string $title;
    private string $description;
    private string $imageurl;
    private Coordinates $coordinates;

    /**
     * @param string $id
     * @param string $title
     * @param string $description
     * @param string $imageurl
     * @param Coordinates $coordinates
     */
    public function __construct(string $id, string $title, string $description, string $imageurl, Coordinates $coordinates=null)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->imageurl = $imageurl;
        $this->coordinates = $coordinates;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getImageurl(): string
    {
        return $this->imageurl;
    }

    /**
     * @param string $imageurl
     */
    public function setImageurl(string $imageurl): void
    {
        $this->imageurl = $imageurl;
    }

    /**
     * @return Coordinates
     */
    public function getCoordinates(): Coordinates
    {
        return $this->coordinates;
    }

    /**
     * @param Coordinates $coordinates
     */
    public function setCoordinates(Coordinates $coordinates): void
    {
        $this->coordinates = $coordinates;
    }
}