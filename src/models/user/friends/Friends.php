<?php

require_once 'src/models/user/authentication/User.php';

class Friends
{
    private $friends=[];

    public function __construct()
    {
    }

    /**
     * @return array
     */
    public function getFriends(): array
    {
        return $this->friends;
    }

    public function addFriend(User $user) {
        array_push($this->friends, $user);
    }

    /**
     * @param array $friends
     */
    public function setFriends(array $friends): void
    {
        $this->friends = $friends;
    }
}