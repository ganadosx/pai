<?php

require_once 'AppController.php';
require_once 'src/models/route/Route.php';
require_once 'src/models/route/Routes.php';
require_once 'src/models/route/coordinate/Coordinates.php';
require_once 'src/models/route/coordinate/Coordinate.php';
require_once 'src/repository/routes/RoutesRepository.php';


class RoutesController extends AppController
{
    private $messages = [];
    private RoutesRepository $routesRepository;

    public function __construct()
    {
        parent::__construct();
        $this->routesRepository = new RoutesRepository();
    }

    public function routes(){
        $allRoutes = $this->routesRepository->getRoutes();

        $this->render('routes', [
            'allRoutes' => $allRoutes]);
    }

}