const search = document.querySelector('input[placeholder="find friend"]');
const friendsContainer = document.querySelector(".friends");

search.addEventListener('keyup', function(event) {
    if(event.key === "Enter"){
        event.preventDefault();

        const data = {search: this.value};

        fetch("/search", {
            method: "POST",
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function(response) {
            return response.json();
        }).then(function(friends){
           friendsContainer.innerHTML = "";
           loadFriends(friends)
        });
    }
});

function loadFriends(friends) {
    friends.forEach(friend => {
        console.log(friend);
        createFriend(friend);
    })
}


function createFriend(friend) {
    const template = document.querySelector("#friends-template");
    const clone = template.content.cloneNode(true);

    const image = clone.querySelector("img");
    image.src = `${friend.imageurl}`;
    const name = clone.querySelector("h2");
    username = friend.firstname + " " + friend.lastname;
    name.innerHTML = username;
    const email = clone.querySelector("h3");
    email.innerHTML = friend.email;

    friendsContainer.appendChild(clone);
}