<!<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/settings.css">
    <script src="https://kit.fontawesome.com/ac5ad3ed5c.js" crossorigin="anonymous"></script>
    <title>PROJECTS</title>
</head>
<body>
<div class="base-container">
    <nav>
        <img src="public/img/logo.svg">
        <ul>
            <hr id="hr-under-logo">
            <li>
                <i class="fas fa-route"></i>
                <a href="/routes" class="button">Choose a route</a>
            </li>
            <li>
                <i class="fas fa-map"></i>
                <a href="/map" class="button">Map</a>
            </li>
            <li>
                <i class="fas fa-user-friends"></i>
                <a href="/friends" class="button">Friends</a>
            </li>
            <li>
                <i class="fas fa-bell"></i>
                <a href="/news" class="button">News</a>
            </li>
            <hr id="hr-under-menu">
            <li>
                <i class="fas fa-cog"></i>
                <a href="/settings" class="button">Settings</a>
            </li>
        </ul>
    </nav>
    <main>
        <section class="settings-part">
            <div id="user-data">
                <button class="button button-center-text" id="email-address" disabled>email@example.com</button>
                <button class="button button-center-text" id="first-name" disabled>John</button>
                <button class="button button-center-text" id="last-name" disabled>Doe</button>
                <button class="button button-center-text" id="change-password">change password</button>
            </div>
        </section>
        <section class="stats-part">
            <form action="uploadImage" method="POST" enctype="multipart/form-data">
                <input name="file" type="file">
                <button class="button save-image-button" type="submit">save</button>
            </form>
        </section>
    </main>
</div>

</body>
</html>