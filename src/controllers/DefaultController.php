<?php

require_once 'AppController.php';

require_once __DIR__ . '/../models/Person.php';


class DefaultController extends AppController
{
    public function login()
    {
        $this->render('login');
    }

    public function register()
    {
        $this->render('register');
    }

    public function routes()
    {
        $this->render('routes');
    }

    public function friends()
    {
        $person1 = new Person(1, "John", "Doe", "John from doe", 'https://images.unsplash.com/photo-1633113088092-3460c3c9b13f?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=870&q=80');
        $person2 = new Person(2, "Ed", "Else", "FB");
        $person3 = new Person(3, "George", "Delse", "RT");
        $person4 = new Person(4, "Peter", "Patrick", "2077", 'https://images.unsplash.com/photo-1633113088092-3460c3c9b13f?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=870&q=80');
        $person5 = new Person(5, "Patrick", "Sasha", "2077");
        $person6 = new Person(6, "Sasha", "Doe", "2077");
        $person7 = new Person(7, "Dura", "John", "2077");

        $this->render('friends', [
            'friends' => [$person1, $person2, $person3, $person4, $person5, $person6, $person7]
        ]);
    }

    public function news()
    {
        $this->render('news');
    }

    public function map()
    {
        $this->render('map');
    }

    public function settings()
    {
        $this->render('settings');
    }
}