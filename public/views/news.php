<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/news.css">
    <script src="https://kit.fontawesome.com/ac5ad3ed5c.js" crossorigin="anonymous"></script>
    <title>PROJECTS</title>
</head>
<body>
<div class="base-container">
    <nav>
        <img src="public/img/logo.svg">
        <ul>
            <hr id="hr-under-logo">
            <li>
                <i class="fas fa-route"></i>
                <a href="/routes" class="button">Choose a route</a>
            </li>
            <li>
                <i class="fas fa-map"></i>
                <a href="/map" class="button">Map</a>
            </li>
            <li>
                <i class="fas fa-user-friends"></i>
                <a href="/friends" class="button">Friends</a>
            </li>
            <li>
                <i class="fas fa-bell"></i>
                <a href="/news" class="button">News</a>
            </li>
            <hr id="hr-under-menu">
            <li>
                <i class="fas fa-cog"></i>
                <a href="/settings" class="button">Settings</a>
            </li>
        </ul>
    </nav>
    <main>
        <header>
            <div class="search-bar">
                <form>
                    <input placeholder="find news">
                </form>
            </div>
            <div class="add-news">
                <button id="add-news-button" disabled><i class="fas fa-plus"></i>add news</button>
            </div>
        </header>
        <section class="news">
            <?php foreach ($allNews->getAllNews() as $newsDetails): ?>
                <div id="news-<?= $newsDetails->getId() ?>">
                    <button class="see-more-button">See more</button>
                    <div>
                        <div>
                            <h2><?= $newsDetails->getTitle() ?></h2>
                            <p>
                                <?= $newsDetails->getDescription() ?>
                            </p>
                        </div>
                        <img src="public/uploads/news/<?= $newsDetails->getImageurl() ?>" alt="image">
                    </div>
                </div>
            <?php endforeach; ?>
        </section>
    </main>
</div>

</body>