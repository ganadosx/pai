<?php

require_once 'src/repository/Repository.php';
require_once 'src/models/news/AllNews.php';
require_once 'src/models/news/News.php';

class NewsRepository extends Repository
{
    public function getAllNews(): ?AllNews
    {
        $stmt = $this->database->connect()->prepare(
            '
                SELECT * FROM public.news;
            '
        );
        $stmt->execute();
        $news = $stmt->fetchAll(PDO::FETCH_ASSOC);



        $allNews = new AllNews();

        if ($news == false) {
            return null;
        }

        foreach ($news as $row) {
            $allNews->addNews(new News(
                    $row['id_news'],
                    $row['imageurl'],
                    $row['description'],
                    $row['title'])
            );
        }

        return $allNews;
    }

    public function getNewsById(int $id): ?News
    {
        $stmt = $this->database->connect()->prepare(
            '
                SELECT * FROM public.news WHERE id_news = :id
            '
        );
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $news = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($news == false) {
            return null;
        }

        return new News(
            $news['id_news'],
            $news['imageurl'],
            $news['description'],
            $news['title']
        );
    }

    public function saveNews(News $news): string
    {
        $id = $news->getId();
        if ($this->getNewsById($id) != null) {
            return "News already exist";
        }
        try {
            $stmt = $this->database->connect()->prepare(
                '
                INSERT INTO public.news(id_news, imageurl, description)
                VALUES (:id, :imageurl, :description)
            '
            );

            $imageurl = $news->getImageurl();
            $description = $news->getDescription();

            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->bindParam(':imageurl', $imageurl, PDO::PARAM_STR);
            $stmt->bindParam(':description', $description, PDO::PARAM_STR);

            $stmt->execute();

            return "News created successfully";
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }
}