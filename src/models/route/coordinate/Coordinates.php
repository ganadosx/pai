<?php

require_once 'src/models/route/coordinate/Coordinate.php';

class Coordinates
{
    private $coordinates=[];

    public function __construct()
    {
    }

    public function addCoordinates(Coordinate $coordinate): void {
        array_push($this->coordinates, $coordinate);
    }

    /**
     * @return array
     */
    public function getCoordinates(): array
    {
        return $this->coordinates;
    }

    /**
     * @param array $coordinates
     */
    public function setCoordinates(array $coordinates): void
    {
        $this->coordinates = $coordinates;
    }

}