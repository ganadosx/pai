<?php

require_once 'AppController.php';
require_once 'src/models/user/authentication/User.php';
require_once 'src/repository/user/UserRepository.php';

class UploadPersonImageController extends AppController
{
    private const MAX_FILE_SIZE = 1024*1024;
    private const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    private const UPLOAD_DIRECTORY = '/../public/uploads/';

    private $messages = [];
    private UserRepository $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }


    public function uploadImage()
    {
        if($this->isPost() && is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file'])){

            move_uploaded_file(
                $_FILES['file']['tmp_name'],
                dirname(__DIR__).self::UPLOAD_DIRECTORY.$_FILES['file']['name']
            );

            $path = "public/uploads/".$_FILES['file']['name'];
            setCookie('imageurl', $path, time() + (86400 * 30), "/");
            $this->updateImage($path);

            return $this->render('settings', ['messages' => $this->messages, 'imagePath' => $_FILES['file']['name']]);
        }

        return $this->render('uploadImage', ['messages' => $this->messages]);
    }

    private function updateImage(string $path) {
        $email = $_COOKIE['user'];
        $user = $this->userRepository->getUser($email);
        $user->setImageUrl($path);
        $this->userRepository->updateUserImage($user);
    }

    private function validate(array $file): bool {
        if($file['size'] > self::MAX_FILE_SIZE) {
            $this->messages[] = 'FIle is too large for destination filesystem';
            return false;
        }
        if(!isset($file['type']) && !in_array($file['type'], self::SUPPORTED_TYPES)) {
            $this->messages[] = 'FIle type is not supported';
            return false;
        }
        return true;
    }
}
