<?php

require_once 'src/models/route/Route.php';

class Routes
{
    private $routes=[];

    public function __construct()
    {
    }

    public function addRoutes(Route $route): void {
        array_push($this->routes, $route);
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }

    /**
     * @param array $routes
     */
    public function setRoutes(array $routes): void
    {
        $this->routes = $routes;
    }

}