<?php

require_once 'AppController.php';
require_once 'src/models/news/News.php';
require_once 'src/repository/news/NewsRepository.php';

class NewsController extends AppController
{
    private $messages = [];
    private NewsRepository $newsRepository;

    public function __construct()
    {
        parent::__construct();
        $this->newsRepository = new NewsRepository();
    }


//    public function getNews(int $id){
//        $news = $this->newsRepository->getNewsById($id);
//
//        $this->render('news', [
//            'news' => [$news]]);
//    }

    public function news(){
        $allNews = $this->newsRepository->getAllNews();

        $this->render('news', [
            'allNews' => $allNews]);
    }
}