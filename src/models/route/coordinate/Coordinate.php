<?php

class Coordinate
{
    private string $x;
    private string $y;

    /**
     * @param string $x
     * @param string $y
     */
    public function __construct(string $x, string $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * @return string
     */
    public function getX(): string
    {
        return $this->x;
    }

    /**
     * @param string $x
     */
    public function setX(string $x): void
    {
        $this->x = $x;
    }

    /**
     * @return string
     */
    public function getY(): string
    {
        return $this->y;
    }

    /**
     * @param string $y
     */
    public function setY(string $y): void
    {
        $this->y = $y;
    }

}