<?php

require_once 'AppController.php';
require_once 'src/models/user/authentication/User.php';
require_once 'src/models/user/friends/Friends.php';
require_once 'src/repository/friends/FriendsRepository.php';

class FriendsController extends AppController
{
    private $messages = [];
    private FriendsRepository $friendsRepository;

    public function __construct()
    {
        parent::__construct();
        $this->friendsRepository = new FriendsRepository();
    }

    public function friends()
    {
        $allFriends = $this->friendsRepository->getFriends($_COOKIE['user']);

        $this->render('friends', [
            'allFriends' => $allFriends,
            'messages' => $this->messages]);
    }

    public function search()
    {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if ($contentType === "application/json") {
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            header('Content-Type: application/json');
            http_response_code(200);

            echo json_encode($this->friendsRepository->getFriendsByTitle($_COOKIE['user'], $decoded['search']));
        }
    }
}