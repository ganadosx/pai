<!<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/settings.css">
    <script src="https://kit.fontawesome.com/ac5ad3ed5c.js" crossorigin="anonymous"></script>
    <title>PROJECTS</title>
</head>
<body>
<div class="base-container">
    <nav>
        <img src="public/img/logo.svg">
        <ul>
            <hr id="hr-under-logo">
            <li>
                <i class="fas fa-route"></i>
                <a href="/routes" class="button">Choose a route</a>
            </li>
            <li>
                <i class="fas fa-map"></i>
                <a href="/map" class="button">Map</a>
            </li>
            <li>
                <i class="fas fa-user-friends"></i>
                <a href="/friends" class="button">Friends</a>
            </li>
            <li>
                <i class="fas fa-bell"></i>
                <a href="/news" class="button">News</a>
            </li>
            <hr id="hr-under-menu">
            <li>
                <i class="fas fa-cog"></i>
                <a href="/settings" class="button">Settings</a>
            </li>
        </ul>
    </nav>
    <main>
        <section class="settings-part">
            <div id="user-data">
                <button class="button button-center-text" id="email-address" disabled><?= $_COOKIE['user'] ?></button>
                <button class="button button-center-text" id="first-name" disabled><?= $_COOKIE['firstname'] ?></button>
                <button class="button button-center-text" id="last-name" disabled><?= $_COOKIE['lastname'] ?></button>
                <button class="button button-center-text" id="change-password">change password</button>
                <form class="logout" action="logout" method="POST">
                    <button class="button button-center-text" type="submit">Log Out</button>
                </form>
            </div>
            <div id="user-image">
                <div id="upload-image-messages"></div>
                <img alt = "person.img" src=<?= $_COOKIE['imageurl'] ?>>
                <a href="uploadImage">Upload an image</a>
            </div>
        </section>
        <section class="stats-part">
            <div id="stats">
                <div class="stats-div" id="stats1">distance</div>
                <div class="stats-div" id="stats-value1">1234325km</div>

                <div class="stats-div" id="stats2">distance</div>
                <div class="stats-div" id="stats-value2">1234325km</div>

                <div class="stats-div" id="stats3">distance</div>
                <div class="stats-div" id="stats-value3">1234325km</div>

                <div class="stats-div" id="stats4">distance</div>
                <div class="stats-div" id="stats-value4">1234325km</div>

                <div class="stats-div" id="stats5">distance</div>
                <div class="stats-div" id="stats-value5">1234325km</div>

                <div class="stats-div" id="stats6">distance</div>
                <div class="stats-div" id="stats-value6">1234325km</div>

                <div class="stats-div" id="stats7">distance</div>
                <div class="stats-div" id="stats-value7">1234325km</div>

                <div class="stats-div" id="stats8">distance</div>
                <div class="stats-div" id="stats-value8">1234325km</div>
            </div>
        </section>
    </main>
</div>

</body>
</html>