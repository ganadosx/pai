<?php

require_once 'src/models/news/News.php';

class AllNews
{
    private $allNews=[];

    public function __construct()
    {
    }

    public function addNews(News $news): void {
        array_push($this->allNews, $news);
    }


    /**
     * @return mixed
     */
    public function getAllNews()
    {
        return $this->allNews;
    }

    /**
     * @param mixed $allNews
     */
    public function setAllNews($allNews): void
    {
        $this->allNews = $allNews;
    }
}