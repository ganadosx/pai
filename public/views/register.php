<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/background.css">
    <link rel="stylesheet" type="text/css" href="public/css/register.css">

    <script type="text/javascript" src="public/js/register.js" defer></script>
    <title>REGISTER</title>
</head>
<body>
    <div class="bg-img"></div>
    <div class="container">
        <div class="logo">
            <img src = "public/img/logo.svg">
        </div>
        <div class="login-container">
            <div class="message">
                <?php if(isset($messages)) {
                    foreach($messages as $message) {
                        echo $message;
                    }
                }
                ?>
            </div>
            <form class="login" action="register" method="POST">
                <input name="email" type="text" placeholder="email@example.com">
                <input name="password" type="password" placeholder="password">
                <input name="confirmPassword" type="password" placeholder="password">
                <input name="firstname" type="text" placeholder="firstname">
                <input name="lastname" type="text" placeholder="lastname">
                <div class="spec-buttons">
                    <button name="register_user" type="submit" id="register">REGISTER</button>
                    <button id="back-to-sign-in"><a href="/login">BACK TO SIGN IN</a></button
                </div>
            </form>
        </div>
    </div>
</body>