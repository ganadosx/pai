<!doctype html>
<html lang="">
<head>
    <title>PROJECTS</title>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/map.css">
    <link href="https://api.mapbox.com/mapbox-gl-js/v2.6.0/mapbox-gl.css" rel="stylesheet">

    <script src="https://kit.fontawesome.com/ac5ad3ed5c.js" crossorigin="anonymous"></script>
    <script src="https://api.mapbox.com/mapbox-gl-js/v2.6.0/mapbox-gl.js"></script>
</head>
<body>
<div class="base-container">
    <nav>
        <img src="public/img/logo.svg">
        <ul>
            <hr id="hr-under-logo">
            <li>
                <i class="fas fa-route"></i>
                <a href="/routes" class="button">Choose a route</a>
            </li>
            <li>
                <i class="fas fa-map"></i>
                <a href="/map" class="button">Map</a>
            </li>
            <li>
                <i class="fas fa-user-friends"></i>
                <a href="/friends" class="button">Friends</a>
            </li>
            <li>
                <i class="fas fa-bell"></i>
                <a href="/news" class="button">News</a>
            </li>
            <hr id="hr-under-menu">
            <li>
                <i class="fas fa-cog"></i>
                <a href="/settings" class="button">Settings</a>
            </li>
        </ul>
    </nav>
    <main>
        <section class="maps">
            <div id="map">
            </div>
                <script>
                    mapboxgl.accessToken = "pk.eyJ1IjoiZ2FuYWRvcyIsImEiOiJja3dpYm9zMGIwY2cwMnZsZHUxZHppZGxiIn0.AXH3yQJvQMnfRzWuXA5n7w";
                    const map = new mapboxgl.Map({
                        container: 'map', // container ID
                        style: 'mapbox://styles/mapbox/streets-v11', // style URL
                        center: [-74.5, 40], // starting position [lng, lat]
                        zoom: 9 // starting zoom
                    });
                </script>
        </section>
    </main>
</div>
</body>
</html>


<!-- TODO:-->
<!-- mapbox -> tutorials/custom-markers-gl-js/-->
<!-- -->