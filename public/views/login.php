<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/background.css">
    <title>LOGIN PAGE</title>
</head>
<body>
    <div class="bg-img"></div>
    <div class="container">
        <div class="logo">
            <img src = "public/img/logo.svg">
        </div>
        <div class="login-container">
            <form class="login" action="login" method="POST">
                <div class="message">
                    <?php if(isset($messages)) {
                        foreach($messages as $message) {
                            echo $message;
                        }
                    }
                    ?>
                </div>
                <input name="email" type="text" placeholder="email@example.com">
                <input name="password" type="password" placeholder="password">
                <div class="spec-buttons">
                    <button type="submit" id="login">LOGIN</button>
                    <button id="register"><a href="/register">REGISTER</a></button>
                </div>
            </form>
        </div>
    </div>
</body>