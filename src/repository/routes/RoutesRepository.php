<?php

require_once 'src/repository/Repository.php';
require_once 'src/models/route/Route.php';
require_once 'src/models/route/Routes.php';
require_once 'src/models/route/coordinate/Coordinates.php';
require_once 'src/models/route/coordinate/Coordinate.php';

class RoutesRepository extends Repository
{

    public function getRoutes(): ?Routes
    {
        $stmt = $this->database->connect()->prepare(
            '
                SELECT * FROM public.routes;
            '
        );
        $stmt->execute();

        $routes = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($routes == false) {
            return null;
        }

        $allRoutes = new Routes();

        foreach ($routes as $row) {
            $allRoutes->addRoutes(new Route(
                $row['id_route'],
                $row['title'],
                $row['description'],
                $row['imageurl'],
                $this->getRouteCoordinates($row['id_route'])
            ));
        }

        return $allRoutes;
    }

    public function getRoute(int $id): ?Route
    {
        $stmt = $this->database->connect()->prepare(
            '
                SELECT * FROM public.routes WHERE id_route = :id;
            '
        );
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $route = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($route == false) {
            return null;
        }

        return new Route(
            $route['id_route'],
            $route['title'],
            $route['description'],
            $route['imageurl'],
            $this->getRouteCoordinates($id)
        );
    }

    public function saveRoute(Route $route): string
    {
        try {
            $stmt = $this->database->connect()->prepare(
                '
                INSERT INTO public.route(description)
                VALUES (:description) RETURNING id_route
            '
            );
            $description = $route->getDescription();

            $stmt->bindParam(':description', $description, PDO::PARAM_STR);

            $stmt->execute();

            $id = $stmt['id_route'];

//            TODO zapisywanie cooridnates do tabeli i pozniej to do tabeli posredniczacej
//            TODO zmiana zapisu do bazy: Coordinates jako JSON

            $messageCoords = $this->saveCoordinates($route->getCoordinates());

            return "Route created successfully";
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    private function getRouteCoordinates(int $id): ?Coordinates
    {
        $stmt = $this->database->connect()->prepare(
            '
                SELECT * FROM public.routes WHERE id_route = :id;
            '
        );
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $xy = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $coordinates = new Coordinates();

        foreach ($xy as $record) {
            $coordinate = new Coordinate(floatval($record['x']), floatval($record['y']));
            $coordinates->addCoordinates($coordinate);
        }
        return $coordinates;
    }

    private function saveCoordinates(Coordinates $coordinates): string
    {
        try {
            $stmt = $this->database->connect()->prepare(
                '
                INSERT INTO public.coordinates(x, y)
                SELECT *
                FROM unnest( :coords )
            '
            );
            $coords = $coordinates->getCoordinates();

            $stmt->bindParam(':cords', $coords);

            $stmt->execute();

            return "Coordinates added successfully.";
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }
}