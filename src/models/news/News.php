<?php

class News
{
    private string $id;
    private string $imageurl;
    private string $description;
    private string $title;

    /**
     * @param string $id
     * @param string $imageurl
     * @param string $description
     * @param string $title
     */
    public function __construct(string $id, string $imageurl, string $description, string $title)
    {
        $this->id = $id;
        $this->imageurl = $imageurl;
        $this->description = $description;
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getImageurl(): string
    {
        return $this->imageurl;
    }

    /**
     * @param string $imageurl
     */
    public function setImageurl(string $imageurl): void
    {
        $this->imageurl = $imageurl;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

}